//
// replacement lid for Hammond 1591D box
// opto-isolator for MGH
//


$fn=64;
e=0.1;

tenth = 2.54;
// jprs = tenth/5;
jprs = 0.2;

module peg( hg) {
     cylinder( h=hg, r=3.5);
}

// box outer dimensions
out_x = 150;
out_y = 80;

// box mounting hole spacing
hole_x = 136.2;
hole_y = 66.2;

// inner size of box rim
edge_x = 145.61;
edge_y = 75.61;

// out pcb dimensions
pcb_x = 143;
pcb_y = 73;

// pcb "tab" dimensions
pcb_xtab = 128;
pcb_ytab = 58;

// pcb corner coutout size
corner_x = (pcb_x-pcb_xtab)/2;
corner_y = (pcb_y-pcb_ytab)/2;

// PCB origin offset from box outer dimensions
pcb_off_x = (out_x-pcb_x)/2;
pcb_off_y = (out_y-pcb_y)/2;

// size of cutout and gap for on/off switch
switch_x = 5;
switch_y = 6;
switch_gap = 0.5;

sw_x0 = (switch_x+switch_gap*2)/2;
sw_y0 = (switch_y+switch_gap*2)/2;

// led hole dia
led_hole_dia = 3.8;

// offset to PCB mounting holes from origin
left_hole_dx = pcb_off_x + 37.465 - 32.5;
right_hole_dx = pcb_off_x + 161.29 - 32.5;
		 
// vertical spacing between mounting holes, left and right
left_hole_v_spc = 47.0;
right_hole_v_spc = 63.5;


left_hole_dy = pcb_off_y + (pcb_y-left_hole_v_spc)/2;
right_hole_dy = pcb_off_y + (pcb_y-right_hole_v_spc)/2;

// offset to box lid mounting holes
hole_dx = (out_x-hole_x)/2;
hole_dy = (out_y-hole_y)/2;

// offset to inside of box rim
edge_dx = (out_x-edge_x)/2;
edge_dy = (out_y-edge_y)/2;

// extension hight inside lid
peg_z = 10;
// thickness of lid surface
lid_z = 2;

// drill hole depth (not critical, for now drill thru all)
thru_hole = peg_z+lid_z+1+20;

module rim() {
     difference() {
	  cube( [out_x, out_y, peg_z]);
	  translate( [edge_dx, edge_dy, -e])
	       cube( [edge_x, edge_y, peg_z+2*e]);
     }
}


module pcb() {
     color("green")
     difference() {
	  cube( [pcb_x, pcb_y, 1.6]);
	  translate( [0, 0, -e]) {
	       translate( [-e, -e, 0]) cube( [ corner_x+e, corner_y+e, 1.6+2*e]);
	       translate( [pcb_x-corner_x, -e, 0]) cube( [ corner_x+e, corner_y+e, 1.6+2*e]);
	       translate( [-e, pcb_y-corner_y, 0]) cube( [ corner_x+e, corner_y+e, 1.6+2*e]);
	       translate( [pcb_x-corner_x, pcb_y-corner_y, 0]) cube( [ corner_x+e, corner_y+e, 1.6+2*e]);
	  }
     }
}


module lid() {

     difference() {
	  union() {
	       // surface of lid
	       cube( [out_x, out_y, lid_z]);
	       // mounting hole peg extensions
	       translate( [0, 0, -peg_z]) {
		    translate( [hole_dx, hole_dy, 0]) peg(peg_z);
		    translate( [out_x-hole_dx, hole_dy, 0]) peg(peg_z);
		    translate( [hole_dx, out_y-hole_dy, 0]) peg(peg_z);
		    translate( [out_x-hole_dx, out_y-hole_dy, 0]) peg(peg_z);
	       }
//	       translate( [pcb_off_x, pcb_off_y, 10])  pcb();

	  }
	  // mounting holes
	  translate( [0, 0, -peg_z-e]) {
	       translate( [hole_dx, hole_dy, 0]) cylinder( h=peg_z+lid_z+2*e, d=3.8);
	       translate( [out_x-hole_dx, hole_dy, 0]) cylinder( h=peg_z+lid_z+2*e, d=3.8);
	       translate( [hole_dx, out_y-hole_dy, 0]) cylinder( h=peg_z+lid_z+2*e, d=3.8);
	       translate( [out_x-hole_dx, out_y-hole_dy, 0]) cylinder( h=peg_z+lid_z+2*e, d=3.8);
	  }

	  // pcb holes
	  translate( [0, 0, -e]) {

	       // pcb holes
	       translate( [0, 0, -e]) {
		    translate( [left_hole_dx, left_hole_dy, 0]) cylinder( h=thru_hole, d=4.3);
		    translate( [left_hole_dx, left_hole_dy+left_hole_v_spc, 0]) cylinder( h=thru_hole, d=4.3);
		    translate( [right_hole_dx, right_hole_dy, 0]) cylinder( h=thru_hole, d=4.3);
		    translate( [right_hole_dx, right_hole_dy+right_hole_v_spc, 0]) cylinder( h=thru_hole, d=4.3);

		    translate( [out_x-left_hole_dx, left_hole_dy, 0]) cylinder( h=thru_hole, d=4.3);
		    translate( [out_x-left_hole_dx, left_hole_dy+left_hole_v_spc, 0]) cylinder( h=thru_hole, d=4.3);
		    translate( [out_x-right_hole_dx, right_hole_dy, 0]) cylinder( h=thru_hole, d=4.3);
		    translate( [out_x-right_hole_dx, right_hole_dy+right_hole_v_spc, 0]) cylinder( h=thru_hole, d=4.3);

	       }

	       // BNC hole
	       translate( [pcb_off_x+20.56, pcb_off_y+34.42, 0])  cylinder( h=thru_hole, d=13);

	       // output LED hole
	       translate( [pcb_off_x+20.56, pcb_off_y+16.64, 0])  cylinder( h=thru_hole, d=led_hole_dia);

	       // poer LED hole
	       translate( [pcb_off_x+62.47, pcb_off_y+61.09, 0])  cylinder( h=thru_hole, d=led_hole_dia);

	       // jumper opening
	       translate( [pcb_off_x+43.42, pcb_off_y+34.42, 0])
		    translate( [-tenth/2-jprs, -tenth*1.5-jprs, 0])
		    cube( [tenth+jprs*2, jprs*2+tenth*3, thru_hole]);

	       // tip jacks for input
	       tipd = 10.0;
	       translate( [pcb_off_x+133.6, pcb_off_y+28.1, 0])
		 cylinder( d=tipd, h=thru_hole);
	       translate( [pcb_off_x+133.6, pcb_off_y+43.3, 0])
		 cylinder( d=tipd, h=thru_hole);

	       
	       // switch opening
	       swid = 6.86;
	       slen = 12.7;
	       translate( [pcb_off_x+51.04, pcb_off_y+60.71])
		    translate( [-swid/2-jprs, -slen/2-jprs, 0])
		    cube( [swid+jprs*2, slen+jprs*2, thru_hole]);
	       
	       // barrel jack opening
	       bwid = 9;
	       blen = 10.7;
	       translate( [pcb_off_x+32.6, pcb_off_y+58.55]) {
		    translate( [-bwid/2+jprs, -4.2-jprs, 0])
			 cube( [bwid+jprs*2, blen+jprs*2, thru_hole]);
	       }
	  }
     }

     // rim
     translate( [0, 0, -peg_z])
	  rim();
}

// flip for slicing
// translate( [0, out_y, 0]) rotate( [180, 0, 0]) lid();

// 2D version
projection() { lid(); }
