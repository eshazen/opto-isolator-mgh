//
// CIT switch SPDS ANT11SF1CQE
//

$fn = 64;
e=0.1;

wid = 6.86;
len = 12.7;
hgt = 8.89;

bshl = 7.1;
bshd = 5.7;    // guess

actl = 10.41;
actd = 2.92;

pind = 0.76;
pinl = 4.8;
pins = 4.7;

module sw() {
     // body
     color("red")
	  translate( [-wid/2, -len/2, 0])
	  cube( [wid, len, hgt]);
     // bushing
     color("silver")
	  translate( [0, 0, hgt-e])
	  cylinder( h=bshl+e, d=bshd);
     // actuator
     color("grey")
	  translate( [0, 0, hgt+bshl-e])
	  rotate( [10, 0, 0])
	  cylinder( h=actl, d=actd);
     // pins
     color("gold") {
	  translate( [0, 0, -pinl-e]) {
	       translate( [0, -pins, 0])
		    cylinder( h=pinl, d=pind);
	       translate( [0, pins, 0])
		    cylinder( h=pinl, d=pind);
	       cylinder( h=pinl, d=pind);
	  }
     }
}


sw();
