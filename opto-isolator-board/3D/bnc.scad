//
// Molex 0731711900 vertical PCB mount BNC
//

$fn=64;
e = 0.1;

wall = 0.5;

module bnc() {
     color( "silver") {

	  cylinder( h=5.08, d=11);

	  difference() {
	       union() {
		    cylinder( h=16.6, d=9.7);
		    translate( [0, 5.5, 13])
			 rotate( [90, 0, 0])
			 cylinder( h=11, d=2);
	       }
	       translate( [0, 0, -e])
		    cylinder( h=16.6+2*e, d=9.7-2*wall);
	  }

	  difference() {
	       cylinder( h=14, d=3);
	       translate( [0, 0, -e])
		    cylinder( h=14+2*e, d=1.3);
	  }

	  pz = 3.18;
	  pxy = 3.43;

	  translate( [0, 0, -pz]) {
	       translate( [pxy, -pxy, 0])
		    cylinder( d=1.9, h=pz+e);
	       translate( [-pxy, -pxy, 0])
		    cylinder( d=1.9, h=pz+e);
	       translate( [pxy, pxy, 0])
		    cylinder( d=1.9, h=pz+e);
	       translate( [-pxy, pxy, 0])
		    cylinder( d=1.9, h=pz+e);
	       cylinder( d=0.9, h=pz+e);
	  }
     }
}


bnc();
