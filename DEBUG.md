# Debugging notes

I tested the box on about 11/4/22 and unfortunately didn't capture
many scope traces.  I did document that the output triggers with a
minimum 0.25mA, 4us wide pulse.

**On 12/9/22 the customer complained:**

> I tested the circuit for the first time this afternoon with our
> stimulation equipment.
> 
> Everything runs okay, and the feedback light is very nice.
> 
> There is however a major issue with the circuit which needs to be
> addressed. As you recall, the concept of this circuit is so that a DC
> square-wave current immediately generates a 5V TTL pulse. The pulse
> should be immediate, so less than 1-2 ms lag. However, I tested the
> circuit on an oscilloscope and I’m seeing a delay in the TTL output in
> the order of 6-7 ms, which is unacceptable for our application.
> 
> See below a picture of the oscilloscope. What we are seeing is two
> signals. There is the major 10 Hz sine wave signal which is
> corresponding to our electric brain stimulator (via probe which is
> connected to a resistor that measures the voltage drop between the two
> DC leads). The stimulation protocol has been configured so that there
> is a TMS pulse (i.e. the square wave which gets sent to your
> optocoupler) that is timed exactly at the positive peak of the 10 Hz
> sine wave. I used a magnetic field probe attached to our TMS
> stimulator in order to assess the timing delay. The TMS successfully
> fires with the optocoupler, and is configured to fire precisely on the
> rising edge of the 5V TTL. As you can see in the photo, the TMS pulse
> is visible by the sharp step artifacts. The problem however is that
> the pulses are not delivered precisely to the peak, rather after a
> delay which is approximately 6-7 msec (see second picture).
> 
> It seems this is a fixed delay and not one that is jittering, although
> I’m not sure. It still is not ideal, and we would need to address
> this.

![Scope1](CustomerDocs/image002.png)
![Scope2](CustomerDocs/image003.png)

