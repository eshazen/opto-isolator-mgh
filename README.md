# opto-isolator-mgh

This archive contains the design files for an opto-isolator box
fabricated for MGH/Charlestown.  The function is to receive an
isolated current signal of at least 0.5mA from an electrical
stimulator, and to provide a TTL level (5V) isolated signal to drive a
magnetic stimulator.

* [Schematic (PDF)](opto-isolator-board/opto-isolator-board.pdf)
* [PCB View (PNG)](Documentation/pcb_view.png)
* [Block Diagram (PDF)](Documentation/block_diagram_ver2.pdf)
* [Slide show (PDF)](Documentation/print.pdf)

* [Debug Notes](DEBUG.md)

The front panel (photo) has the following connectors/controls:

* Power input.  Accepts a barrel connector with 2.5mm ID and 5.50mm
  OD.  Power required is 5V at about 250mA maximum.
* Power switch and indicator
* Input jacks: Expects a current-mode signal minimum 0.25mA, at least
  4us wide, to trigger the unit.
* Output BNC: Provides a TTL output (driven by one 74HCT14 device).
  This output is capable of source and sinking 4mA maximum.  This will
  *not* drive a 50 ohm load, so if a BNC cable is used it should be
  kept short, and 50 ohm termination must not be used.
* Polarity Jumper: This is a small shorting jumper which can be placed
  over either the top two or bottom two pins of a 3-pin header to
  select normal or inverted output polarity.
* Signal LED:  This provides an indication that a signal is present.
  For short input pulses the LED may not visibly illuminate.

![Front Panel](Documentation/panel_anno.jpg "Front Panel")

**Disassembly:** remove the 4 corner screws (green arrows) only
if the box needs to be opened.  The PCB and all components are
attached to the lid and come out easily.

The circuit is straightforward.  An external, medical-grade AC/DC
converter "brick" provides 5V, which is used to directly power the
output circuit.  A medical-grade isolated DC/DC converter (U3)
provides +/-12V for the input circuit.  An op-amp converts the input
current to a voltage signal which drives the input of the
opto-isolator.  The output of the isolator is buffered by U1A and used
to either directly drive the output ("inverted" mode) or further
inverted by U1B ("normal" mode) to provide a non-inverted output.



## PCB Design

Errata:

There is one error in the schematic/PCB.  The polarity of the
"signal" LED is incorrect.  To Fix:

* Cut traces to both sides of pin 5 
* wire pins 4 and 5 together
* wire from pin 3 to the cut trace from pin 5

See sketch below for details.

![PCB fix](Documentation/pcb_eco.png "PCB Modifications")
